﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using WebApplication50.Core.Repositories; 

namespace WebApplication50.DAL.Repositories
{
    public class RepositoryManager
    {
        private readonly FreightDbContext freightDbContext;
        public RepositoryManager(FreightDbContext freightDbContext)
        {
            this.freightDbContext = freightDbContext; 
        }

        private IFreightRepository _freights;
        public IFreightRepository Freights => _freights ?? (_freights = new FreightRepository(freightDbContext));

        public int SaveChanges()
        {
            return freightDbContext.SaveChanges(); 
        }

        public Task<int> SaveChangesAsync()
        {
            return freightDbContext.SaveChangesAsync(); 
        }

        public ISqlTransaction BeginTransaction(IsolationLevel isolation = IsolationLevel.ReadCommitted)
        {
            return SqlTransaction.Begin(freightDbContext, isolation); 
        }  
    }
}
