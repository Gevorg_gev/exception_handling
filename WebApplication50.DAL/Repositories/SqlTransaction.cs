﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using WebApplication50.Core.Repositories;

namespace WebApplication50.DAL.Repositories
{
    public class SqlTransaction : ISqlTransaction
    {
        private readonly IDbContextTransaction _transaction;

        public SqlTransaction(FreightDbContext context, System.Data.IsolationLevel level)
        {
            _transaction = context.Database.BeginTransaction(level);
        } 

        internal static ISqlTransaction Begin(FreightDbContext freightDbContext, IsolationLevel isolation)
        {
            throw new NotImplementedException(); 
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _transaction.Dispose();
            }
        } 

        public void Commit()
        {
            _transaction.Commit();
        }

        public void Rollback()
        {
            _transaction.Rollback(); 
        }
    }
}
