﻿using System;
using System.Collections.Generic;
using System.Text;
using WebApplication50.Core.Repositories;

namespace WebApplication50.DAL.Repositories
{
    public class FreightRepository : SqlRepositoryBase<Freight>, IFreightRepository
    {
        public FreightRepository(FreightDbContext freightDbContext) : base(freightDbContext) 
        {

        }
    }
}
