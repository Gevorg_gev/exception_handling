﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using WebApplication50.Core.Operations;


namespace WebApplication50.BLL.Operations
{
    public class FreightBL : IFreightBL  
    {
        private readonly FreightDbContext freightDbContext;
        public Freight AddFreights(Freight freight)
        {
            freightDbContext.Freights.Add(freight);
            freightDbContext.SaveChanges();


            return freight;
        }

        public IEnumerable GetFreights()
        {
            throw new NotImplementedException();
        }

        public bool RemoveFreights(int id)
        {
            var rmv = freightDbContext.Freights.Find(id);
            if (rmv == null)
            {
                return false;
            }

            freightDbContext.Freights.Remove(rmv);
            freightDbContext.SaveChanges();


            return true;  
        }

        public bool UpdateFreights(Freight freight, int id)
        {
            var freightToUpdate = freightDbContext.Freights.Find(id);
            if (freightToUpdate == null)
            {
                return false; 
            }

            freightToUpdate.Id = freight.Id;
            freightToUpdate.Massive = freight.Massive;
            freightToUpdate.Name = freight.Name;
            freightToUpdate.Country = freight.Country;
            freightToUpdate.City = freight.City;


            freightDbContext.Freights.Update(freightToUpdate);
            freightDbContext.SaveChanges();


            return true;
        }
    }
}
