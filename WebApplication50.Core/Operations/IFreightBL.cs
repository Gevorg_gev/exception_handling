﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;


namespace WebApplication50.Core.Operations
{
    public interface IFreightBL
    {
        IEnumerable GetFreights();
        bool RemoveFreights(int id); 
        bool UpdateFreights(Freight freight, int id);
        Freight AddFreights(Freight freight);  
    }
}
