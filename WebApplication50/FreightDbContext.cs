using Microsoft.EntityFrameworkCore;

namespace WebApplication50
{
    public class FreightDbContext : DbContext
    {
        public FreightDbContext(DbContextOptions options) : base(options)
        {

        }
        public DbSet<Freight> Freights { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Freight>(x=>
            {
                x.ToTable("Freights");
                x.HasKey(x => x.Id);
                x.Property(x => x.Id).ValueGeneratedOnAdd(); 
            });
            base.OnModelCreating(modelBuilder);
        }
    }
}