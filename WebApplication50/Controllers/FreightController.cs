using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections; 
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication50
{
    [ApiController]
    [Route("[controller]")]
    public class FreightController : ControllerBase
    {
        private readonly FreightDbContext freightDbContext;
        public FreightController(FreightDbContext freightDbContext)
        {
            this.freightDbContext = freightDbContext; 
        }

        private static readonly string[] Freight_Name = new[]
        {
            "Armenian Freight" , "American Freights" , "Chinese Freights" , "Canadan Freights" 
        };

        private static readonly string[] Freight_Country = new[]
        {
            "Armenia" , "United States Of America" , "China" , "Canada" 
        };

        private static readonly string[] Freight_City = new[]
        {
            "Yerevan" , "Washington" , "Beijing" , "Ottava" 
        };

        [HttpGet]
        public IEnumerable GetFreights()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new Freight
            {
                Id = rng.Next(1,5),
                Massive = rng.Next(1,2500),
                Name = Freight_Name[rng.Next(Freight_Name.Length)],
                Country = Freight_Country[rng.Next(Freight_Country.Length)],
                City = Freight_City[rng.Next(Freight_City.Length)] 
            });
        }

        [HttpPost]
        public IActionResult AddFreights([FromBody] Freight freight)
        {
            freightDbContext.Add(freight);


            return Ok(); 
        }

        [HttpDelete("{id}")]
        public IActionResult RemoveFreights([FromRoute] int id)
        {
            freightDbContext.Remove(id);


            return Ok(); 
        }

        [HttpPut("{id}")]
        public IActionResult UpdateFreights([FromBody] Freight freight, [FromRoute] int id)
        {
            freightDbContext.Update(id); 


            return Ok(); 
        }
    }
}